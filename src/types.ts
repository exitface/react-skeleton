import * as History from 'history'
import * as React from "react"

export interface Route {
  path: string
  component: React.ReactElement<any>
}

export interface Action<T>{
  type: string
  payload: T
  error?: boolean
  meta?: any
}

export interface Routing {
  location: History.Location
}

//
// API Data Models
//

export interface UserModel{
  id: number
  firstname: string
}

//
// Components
//

export interface IApp {
  store: any
  auth: any
  authActions: any
  history: History.History
}

export interface IField {
  type?: string
  name?: string
  hint?: string
  label?: string
  size?: number
  maxlength?: number
  placeholder?: string
  value?: any
  onChange: any
  required?: boolean
  checked?: boolean
  options?: { label: string, value: any }[]
  defaultValue?: any
}

