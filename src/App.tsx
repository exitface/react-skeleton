import * as React from "react"
import * as History from 'history'
import { bindActionCreators } from 'redux'
import { Switch, Route, Redirect } from "react-router-dom"
import { connect, Provider }  from 'react-redux'
import { ConnectedRouter, push } from 'react-router-redux'
import { RouteTransition } from 'react-router-transition'
import * as ReactModal from 'react-modal'
import { first, ReduxResource } from 'redux-api-resources'

import routes from './routes'
import authResource  from './store/auth'
import { IApp } from './types'

class App extends React.Component<IApp, {}> {
  render(){
    const { auth } = this.props

    return(
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <Route render={({location, history, match}) => {
            return (
              <RouteTransition
                pathname={location.pathname}
                atEnter={{ opacity: 0 }}
                atLeave={{ opacity: 0 }}
                atActive={{ opacity: 1 }}
              >
                <Switch key={location.key} location={location}>
                { routes.map(({path, component}, key) => <Route exact path={path} component={component} key={key} />) }
                </Switch>
              </RouteTransition>
            )
          }} />
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default connect(
  (state, props: any) => {
    return { auth: state.auth, ...props }
  },
  (dispatch) => ({
    dispatch,
    authActions: bindActionCreators(authResource.actions, dispatch),
  })
)(App)
