import { all } from 'redux-saga/effects'
import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import thunkMiddleware from "redux-thunk"
import { routerReducer, routerMiddleware } from 'react-router-redux'
import * as History from 'history'
import createSagaMiddleware from 'redux-saga'

import auth from './auth'
import users from './users'
import storage from '../services/storage'

const devToolsExt = (window as any).__REDUX_DEVTOOLS_EXTENSION__
  ? (window as any).__REDUX_DEVTOOLS_EXTENSION__()
  : (f: any) => f

function* rootSaga() {
  yield all([
    auth.saga(),
    users.saga(),
  ])
}

const rootReducer = combineReducers({
  auth: auth.reducer,
  users: users.reducer,
  usersForm: users.formReducer,
  routing: routerReducer
})

export default function(history: History.History){
  const persistedState = storage.load()
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(
    rootReducer,
    {
      auth: persistedState.auth,
      orders: persistedState.orders,
      lineItems: persistedState.lineItems,
    },
    compose(
      applyMiddleware(
        thunkMiddleware,
        routerMiddleware(history),
        sagaMiddleware,
      ),
      devToolsExt
    )
  )

  sagaMiddleware.run(rootSaga)

  return store
}
