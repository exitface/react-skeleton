import { call, put, takeLatest } from 'redux-saga/effects'
import { resourceFor } from 'redux-api-resources'

import { Action, UserModel  } from 'src/types'
import api from 'src/api/users'
import storage from 'src/services/storage'

const { actions, types, reducer, formReducer } = resourceFor("users")

function* usersFetch(action: Action<UserModel>) {
  const { payload } = action
  try {
    const resp = yield call(api.show, payload)
    yield put(actions.fetchSuccess(resp.body.user))
  } catch (e) {
    yield put(actions.fetchFailure(e.message))
  }
}

function* usersUpdate(action: Action<{user: UserModel, params: any}>) {
  const { user, params } = action.payload
  try {
    const resp = yield call(api.update, user, params)
    yield put(actions.updateSuccess(resp.body.user))
  } catch (e) {
    yield put(actions.updateFailure(e.message))
  }
}

export default {
  actions,
  types,
  formReducer,
  reducer,
  saga: function*() {
    yield takeLatest(types.fetchStart, usersFetch)
    yield takeLatest(types.updateStart, usersUpdate)
  }
}
