import * as React from "react"
import * as ReactDOM from "react-dom"
import createHistory from 'history/createBrowserHistory'

import './assets/css/index.scss'
import createStore from './store'
import App from './App'

const history = createHistory()
const store = createStore(history)

ReactDOM.render(<App store={store} history={history} />, document.getElementById("app"))

if (module.hot) module.hot.accept()
