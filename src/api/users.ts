import * as request from 'superagent'
import { UserModel } from 'src/types'

export default {
  sign_in: (email: string, password: string) => {
    return request
      .post(`${ process.env.SPREE_BASE }/users/sign_in`)
      .set('Accept', 'application/json')
      .send({ user: { email, password } })
  },

  sign_up: (email: string, password: string, password_confirmation: string) => {
    return request
      .post(`${ process.env.SPREE_BASE }/users/sign_up`)
      .set('Accept', 'application/json')
      .send({ user: { email, password, password_confirmation } })
  },

  show: (user: UserModel) => {
    return request
      .get(`${process.env.SPREE_BASE}/users/${user.id}`)
      .set('Accept', 'application/json')
  },

  update: (user: UserModel, params: any) => {
    return request
      .patch(`${process.env.SPREE_BASE}/users/${user.id}`)
      .set('Accept', 'application/json')
      .send(params)
  },
}
