import * as React from "react"
import { Link } from "react-router-dom"
import { bindActionCreators } from 'redux'
import { connect }  from 'react-redux'

import { Routing } from '../../types'
// import SubSection1 from './components/SubSection1 '
// import SubSection2 from './components/SubSection2 '

class Home extends React.Component<{ routing: Routing }, {}> {
  render() {
    return (
      <div id='home' className='scene'>
        {/* <SubSection1 /> */}
        {/* <SubSection2 /> */}
      </div>
    )
  }
}

export default connect(
  (state, props) => ({ routing: state.routing }),
  (dispatch) => ({})
)(Home)
