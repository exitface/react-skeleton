import * as React from "react"
import { Switch, Route, Redirect } from "react-router-dom"
import { Route as IRoute } from './types'
import { Home } from './scenes'

export default [
  {
    path: '/',
    component: Home,
  }
  /* And so on. */
]

export function pathFor(route: IRoute, params?: any) {
}

export const PrivateRoute = ({ component: Component, show, path } : { component: any, show: () => boolean, path: any }) => (
  <Route path={path} render={props => (
    show() ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

