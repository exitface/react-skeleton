# React App

**Work in progress. This isn't runnable yet**

## General Ideas / Coding Conventions

* Use props as much as possible, use state sparingly. Props should flow from top-level scene components to child "dumb" components

* Prefer stateless function components.

* One default exported React component per file.

* Use React's state for ephemeral state that doesn't affect the app globally. For example, a toggle in some UI element.

* Components, classes and types should be Pascal case (FooBar), everything else camel case (fooBar). JSON in snake case (foo\_bar).

* Use index export aggregates to make importing easier

* `node_module` imports should be placed at the top of the file. Local components should be imported afterwards with a black line separating the imports

* Import from the project root (i.e `import Component from "src/components"`) rather than relative to the file (i.e. `import Component from "../../../components"`). Note: I've seen tsc complain about this in strange/inconsistent ways

* Stylesheet and test file live next to the files they style/test

* Testing via Jest

* Don't store strings directly in code, use react-intl

## Architecture

#### api/

Any API calls go here

#### assets/

Non-code. Images, fonts, global styles

#### components/

Reusable components. Anything that can appear in multiple areas throughout the application. "Dumb", unaware of redux

#### scenes/

Analagous to "Pages" of a traditional website. "Smart" Containers that are aware of redux. May contain a sub-folder of `components` that are unique to this scene

#### store/

Instead of spliting the actions, action types, reducers and sagas in to separate folders, they're all located in the same sub folders here.

This is where the business logic lives.

#### routes.tsx

Everything to do with routing

#### util/

Miscellaneous shared functions

#### type.ts and definitions.ts

Types for typescript and definitions for JS libraries without TypeScript definitions

## Links

http://redux.js.org/

https://redux-saga.js.org/

https://github.com/yahoo/react-intl

https://facebook.github.io/jest/

https://github.com/ReactTraining/react-router/tree/master/packages/react-router-redux

https://alligator.io/react/react-router-map-to-routes/

https://hackernoon.com/my-journey-toward-a-maintainable-project-structure-for-react-redux-b05dfd999b5

https://medium.com/front-end-hacking/the-three-pigs-how-to-structure-react-redux-application-67f5e3c68392

## Running

#### Development

```
yarn install && yarn start
```

#### Production

```
yarn deploy
```
